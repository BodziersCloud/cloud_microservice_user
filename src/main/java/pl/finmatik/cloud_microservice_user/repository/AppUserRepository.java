package pl.finmatik.cloud_microservice_user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.finmatik.cloud_microservice_user.model.AppUser;


public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
