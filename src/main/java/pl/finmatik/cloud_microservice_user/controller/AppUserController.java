package pl.finmatik.cloud_microservice_user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.web.bind.annotation.*;
import pl.finmatik.cloud_microservice_user.model.AppUserDto;
import pl.finmatik.cloud_microservice_user.model.CreateAppUserDto;
import pl.finmatik.cloud_microservice_user.service.AppUserService;

@RestController
@RequestMapping(path = "/user/")
public class AppUserController {

    @Autowired
    private AppUserService appUserService;

    @PutMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public Long putUser(CreateAppUserDto dto) {
        Long appUserId = appUserService.put(dto);

        return appUserId;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public AppUserDto get(@PathVariable(name = "id") Long id){
        return appUserService.getUser(id);
    }
}
