package pl.finmatik.cloud_microservice_user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AppUserDto {
    private String login;
    private String name;
    private String surname;

    public static AppUserDto fromUser(AppUser appUser) {
        return new AppUserDto(appUser.getUsername(), appUser.getFirstName(), appUser.getLastName());
    }

}
