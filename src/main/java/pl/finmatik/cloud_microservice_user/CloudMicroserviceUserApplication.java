package pl.finmatik.cloud_microservice_user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CloudMicroserviceUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudMicroserviceUserApplication.class, args);
    }

}
